<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="style.css">
<title>Insert title here</title>
</head>
<body class="contactus-boby">
	<h2>Contact Us Form</h2>
	<div class="contactus-form">	
		<form id="form1" action="ContactUsServlet" method="post">
		
			<div>
				<p class="para">Full Name</p>
				<input type="text" size="25" name="fullname" required="required" placeholder="Name">
			</div>
			<div>
				<p class="para">E-mail</p>
				<input type="text"  required="required" name="email" placeholder="Email-id">
			</div>
			<div>
				<p class="para">Message</p>
				<textarea rows="5" cols="30" name="message" required="required"></textarea>
			</div>
			<input type="submit" value="SUBMIT" >
		</form>
		
	</div>
</body>
</html>