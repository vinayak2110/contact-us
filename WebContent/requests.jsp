<%@page import="com.contactus.*" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link  rel="stylesheet" href="style.css">
<title>Insert title here</title>
</head>
<body>

	<c:if test="${typeOfList.equals(\"Active Requests\") }">
		<h2 class="table-position">Active Requests</h2>
		
		<c:if test="${updatedList.size() == 0}">
			<h3 class="table-position">No Active Request</h3>
		</c:if>
		
		<c:if test="${updatedList.size() > 0}">
		<div class="table-position">
			<form action="${pageContext.request.contextPath}/RequestsServlet" method="get">
				<table class="content-table">
					<thead>
						<tr>
							<th>Full Name</th>
							<th>Email-ID</th>
							<th>Message</th>
							<th>Status</th>
						<tr>
					</thead>
					<tbody>
						<c:forEach var="contact" items="${updatedList}">
							<tr>
								<td>${contact.getName()}</td>
								<td>${contact.getEmailId()}</td>
								<td>${contact.getMessage()}</td>
								<td><button type="submit" name="button" value="${contact.getRequestId()}">Active</button></td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</form>
		</div>
	</c:if>
	
		<form class="show-form" action="RequestsServlet">	
			<input type="submit" value="Archived Requests" name="typeOfList"/>
		</form>
	</c:if>
	
	<c:if test="${typeOfList.equals(\"Archived Requests\")}">
	
		<h2 class="table-position">Archived Requests</h2>
	
		<c:if test="${updatedList.size() == 0}">
			<h3 class="table-position">No Archived Request</h3>
		</c:if>
		
		<c:if test="${updatedList.size() > 0}">
			<div class="table-position">
				<form action="${pageContext.request.contextPath}/RequestsServlet" method="get">
					<table class="content-table">
						<thead>
							<tr>
								<th>Full Name</th>
								<th>Email-ID</th>
								<th>Message</th>
								<th>Status</th>
							<tr>
						</thead>
						<tbody>
							<c:forEach var="contact" items="${updatedList}">
								<tr>
									<td>${contact.getName()}</td>
									<td>${contact.getEmailId()}</td>
									<td>${contact.getMessage()}</td>
									<td><button type="submit" name="button" value="${contact.getRequestId()}">Archived</button></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</form>
			</div>
		</c:if>
		<form class="show-form" action="RequestsServlet">
			<input type="submit" value="Active Requests" name="typeOfList"/>
		</form>
	</c:if>	
	
</body>
</html>