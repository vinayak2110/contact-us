package com.contactus.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.contactus.Contact;
import com.contactus.dao.RequestsDao;

/**
 * Servlet implementation class RequestsServlet
 */
@WebServlet("/RequestsServlet")
public class RequestsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		RequestsDao requestsDao = new RequestsDao();

		HttpSession httpSession = request.getSession();
		String button = request.getParameter("button");

		String typeOfList = request.getParameter("typeOfList");

		List<Contact> updatedList = new ArrayList<>();

		if (button != null) {
			try {
				String status = requestsDao.changeStatus(button);
				typeOfList = status.equals("1") ? ("Archived Requests") : ("Active Requests");
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}

		if (typeOfList == null || typeOfList.equals("Active Requests")) {
			try {
//				httpSession.setAttribute("typeOfList", "ActiveRequests");

				httpSession.setAttribute("typeOfList", "Active Requests");
				updatedList = requestsDao.getActiveRequests();

			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		} else {
			try {
				httpSession.setAttribute("typeOfList", "Archived Requests");

				updatedList = requestsDao.getArchivedRequests();
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}

		httpSession.setAttribute("updatedList", updatedList);
		response.sendRedirect("requests.jsp");
	}

}
