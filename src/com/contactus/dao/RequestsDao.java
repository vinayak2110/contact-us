package com.contactus.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.contactus.Contact;
import com.contactus.connection.ConnectDB;

public class RequestsDao {

	public String changeStatus(String button) throws ClassNotFoundException, SQLException {

		Connection connection = ConnectDB.getConn();
		Statement statement = connection.createStatement();

		String sql = "select * from contact_us " + "WHERE \"request_id\" = '" + button + "'";

		String status = "";
		ResultSet resultSet = statement.executeQuery(sql);
		resultSet.next();

		if (resultSet.getString(5).equals("1")) {
			sql = "UPDATE contact_us SET \"IsArchived\" = '0' " + "WHERE \"request_id\" = '" + button + "'";

			statement.executeUpdate(sql);
			status = "1";
		} else {
			sql = "UPDATE contact_us SET \"IsArchived\" = '1' " + "WHERE \"request_id\" = '" + button + "'";

			status = "0";
			statement.executeUpdate(sql);
		}

		connection.close();
		statement.close();
		return status;
	}

	public List<Contact> getActiveRequests() throws SQLException, ClassNotFoundException {

		Connection connection = ConnectDB.getConn();
		Statement statement = connection.createStatement();

		List<Contact> contacts = new ArrayList<Contact>();

		ResultSet resultSet = statement.executeQuery("SELECT * FROM contact_us WHERE \"IsArchived\"='0'");

		while (resultSet.next()) {

			Contact contact = new Contact();
			contact.setRequestId(resultSet.getString(1));
			contact.setName(resultSet.getString(2));
			contact.setEmailId(resultSet.getString(3));
			contact.setMessage(resultSet.getString(4));
			contact.setIsArchived(resultSet.getString(5));
			contacts.add(contact);
		}
		return contacts;
	}

	public List<Contact> getArchivedRequests() throws ClassNotFoundException, SQLException {
		Connection connection = ConnectDB.getConn();
		Statement statement = connection.createStatement();

		List<Contact> contacts = new ArrayList<Contact>();

		ResultSet resultSet = statement.executeQuery("SELECT * FROM contact_us WHERE \"IsArchived\"='1'");

		while (resultSet.next()) {

			Contact contact = new Contact();
			contact.setRequestId(resultSet.getString(1));
			contact.setName(resultSet.getString(2));
			contact.setEmailId(resultSet.getString(3));
			contact.setMessage(resultSet.getString(4));
			contact.setIsArchived(resultSet.getString(5));
			contacts.add(contact);
		}
		return contacts;
	}
}