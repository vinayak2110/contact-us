package com.contactus.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import com.contactus.connection.ConnectDB;

public class ContactUsDao {
	
	public void addContact(String fullName,String emailId,String message) throws ClassNotFoundException, SQLException {
		
		
		String sql = "INSERT INTO contact_us (\"Full_Name\",\"Email_ID\",\"Message\")" +
				" VALUES ('"+fullName+"','"+emailId+"','"+message+"')";
		
		Connection connection = ConnectDB.getConn();
		Statement statement = connection.createStatement();
		statement.executeUpdate(sql);
		
		statement.close();
		connection.close();
	}
}
