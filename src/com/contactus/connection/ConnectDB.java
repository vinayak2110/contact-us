package com.contactus.connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectDB {
	
	public static Connection getConn() throws ClassNotFoundException, SQLException {
		
		final String URL = "jdbc:postgresql://localhost:5432/contact_us_data";
		final String USERNAME="postgres";
		final String PASSWORD = "Vinayak@123";
		Connection connection=null;
		
		Class.forName("org.postgresql.Driver");
		
		connection =DriverManager.getConnection(URL, USERNAME, PASSWORD);
		
		return connection;
	}
}